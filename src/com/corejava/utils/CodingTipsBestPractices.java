package com.corejava.utils;

public class CodingTipsBestPractices {

	public static void main(String[] args) {

		//Ctrl+Shif+F : Format the code.
		//Ctrl+Shif+R : Open Resorces
		//Ctrl+D : Delete current cursor line.
		//Ctrl+Q : Move to last edited location.
		//Ctrl+H : To search for a string (select file search from tabs and ensure to have file pattern as * to search on all tile of files.
		//Ctrl+Z/Y Undo and redo
		//syso Cltr+Space
		//Cltr+Space :  For Auto Completion of the code and to read more details for the selected method or variable( check F2 also).
		//Cltr+ MouseSelect on methods for navigating to the deceleration or implementation of the method.
		//Alt+Shift+R //Rename variable and update the references.
		//Alt+Shift+R //Extract strings to constants and see other options also.
		//Alt+ UpArrow or Down Arrow : Moves selected line of code.
		//Crtl+M : To minimize or maximize the view.
		
		// More shortcuts: https://shortcutworld.com/en/Eclipse/win/all
		//TODO : write this for easy of access and a good reminder also.Check in Markers Perspective"Java Task"
		//Auto_Generate toString,GetterSetter
		//Refactor and format code .
		// Do not use all CAPs for abrivating like HTTPRequest is wrong. write as HttpRequest for method or write as HttpRequest as class.
		//Add comments as necessary in the code.
		// Always write meaning full names , don't use general words like myObject, myBook, myName use like id, name firstName,lastName, getBookWithAddress(); updateName();deleteName()
		// Maintain Constants in one file try to extract all strings as constants. Always constants are capitalized.
		// Avoid duplicate code(methods mainly) for common tasks.
		//Add logs whenever necessary.
	}

}
