package com.jitworks.corejava.StaticFinalFinallyFinalize;

import com.company.corJavaApp.A_Variables;

/*QQ Explain about static and its execution flow.
 *QQ Explain about static and final keywords
 *QQ Can we overload static methods in java? Can we override static methods in java?No
 *QQ Why we use static keyword in java?mem
 *QQ Why main method is static in java?Can we write static public void main(String [] args)?
 *QQ When static block is used in Real time applications?properties,config
 * 
 * Static is a Non Access Modifier. The static keyword belongs to the class than instance of the class. can be used to attach a Variable or Method to a Class.

Static keyword CAN be used with:
--------------------------------
Method
Variable
Class nested within another Class
Initialization Block

Static CAN'T be used with:
-------------------------
Class (Not Nested)
Constructor
Interfaces
Method Local Inner Class(Difference then nested class)
Inner Class methods
Instance Variables
Local Variables
--------------------
A static variable is one that's associated with a class, not instance (object) of that class. They are initialized only once , at the start of the execution . 
A single copy to be shared by all instances of the class and it can be accessed directly by the class name and doesn't need any object.

The static initializer is a static {} block of code inside java class, and run only one time before the constructor or main method is called. 
The code block with the static modifier signifies a class initializer; without the static modifier the code block is an instance initializer.

A Static Nested Class can't access enclosing class instance and invoke methods on it, so should be used when the nested class doesn't require access to an instance of the enclosing class . 
A common use of static nested class is to implement a components of the outer object.

Static with Final:
------------------
The global variable which is declared as final and static remains unchanged for the whole execution. 
Because, Static members are stored in the class memory and they are loaded only once in the whole execution. They are common to all objects of the class.
If you declare static variables as final, any of the objects can�t change their value as it is final. Therefore, variables declared as final and static are sometimes referred to as Constants.
All fields of interfaces are referred as constants, because they are final and static by default.
*/
public class F_StaticFinal {



	private static String staticVariable=" Default is null"; //2) can have only one value in any location. no instantiation..
	private String instanceVariable;// this can have many instances at a time
//	int count = 0;// will get memory when instance is created
	static int count = 0;// static variable will get the memory only once, if any object changes the value of the static variable, it will retain its value.
	public F_StaticFinal() {
	       count++;
	        log("Count : " + count);
	}
	static{
		log("In Static Block." + staticVariable);
	}
	//1)static variables and static blocks are executed in an order in which they appear.
	public static void main(String[] args) {
		log("In Static Method." + staticVariable);//3) called before instantiation

		F_StaticFinal fObjRef = new F_StaticFinal();
		log("A_Variables Class and it's static variable : " + A_Variables.AUTO_EXTRACTED_CONSTANT);//Directly call with class name . same for methods
		log("Default staticVariable  is : " + staticVariable + " instanceVariable : " + fObjRef.instanceVariable );
		staticVariable = "Before Meth_A";
		fObjRef.instanceVariable = "inst_before_meth_A";
		log("staticVariable before Meth_A : " + staticVariable + " instanceVariable : " + fObjRef.instanceVariable);
		log("==========================");
		fObjRef.methodA();// non-static so it needs an object.
		log("staticVariable after meth_A : " + staticVariable + " #######instanceVariable #########: " + fObjRef.instanceVariable );
		fObjRef.methodB(fObjRef);//same as calling from ref;
		log("staticVariable after meth_B : " + staticVariable + " instanceVariable : " + fObjRef.instanceVariable );
		log("---------------------------------");
		F_StaticFinal obj1 = new F_StaticFinal();
		F_StaticFinal obj2 = new F_StaticFinal();
		F_StaticFinal obj3 = new F_StaticFinal();
		F_StaticFinal obj4 = new F_StaticFinal();
		log("---------------------------------");

	}

	private static void log(String logMessage) {
		System.out.println(logMessage);
	}

	void methodA() {
		staticVariable = "meth_A";
		F_StaticFinal fObjRef = new F_StaticFinal();
		fObjRef.instanceVariable = "inst_in_meth_A";
		log("methodA called, staticVariable : " + staticVariable + " instanceVariable : " + fObjRef.instanceVariable );
	}

	void methodB( F_StaticFinal fObjRef) {
		staticVariable = "meth_B";
	//	F_StaticFinal fObjRef = new F_StaticFinal();
		fObjRef.instanceVariable = "inst_in_meth_B";
		log("methodB called,  staticVariable : " + staticVariable + " instanceVariable : " + fObjRef.instanceVariable );
	}
	
	static class InnerClass{
		private void syso() {
				log("**Static inner class.");
		}
	}
}
/*
Refer: :
	https://www.javatpoint.com/static-keyword-in-java
	http://javaconceptoftheday.com/final-keyword-in-java/
	http://javaconceptoftheday.com/java-interview-questions-on-final-keyword/
	http://www.instanceofjava.com/2015/07/core-java-interview-questions-on-static.html
		*/
