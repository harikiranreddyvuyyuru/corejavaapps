package com.jitworks.corejava.StaticFinalFinallyFinalize;

import com.corejava.utils.Employee;
//QQ Explain about Final, Finally and finalize? 
//QQ Explain about exception handling in java? can we have only try and finally ? How do we handle exceptions? What is Garbage collection?
//QQ Can we change the value of an interface field? If not, why?
//QQ Can we declare constructors as final?No
//QQ Can we change the state of an object to which a final reference variable is pointing?
//QQ What is the main difference between abstract methods and final methods?
//QQ What is singleton and how to create it?
//LL Explain about java memory management / heap/ stack ? what is young gen memory and per-gen memory?
//LL Learn implementing custom exceptions.
// 
public class G_ExceptionFinalFinallyFinalize extends Employee {
	protected G_ExceptionFinalFinallyFinalize(int empId) {
		super(empId);
	}

	// public class G_FinalFinallyFinalize extends FinalClass { //A- final class cannot be subclassed
	// public class G_ExceptionFinalFinallyFinalize{ //Exception handled in 2 ways , using try catch and using throws clause.
	
	final String finalVariable = "IAM_FINAL";

	@Override
	public void finalize() {
		System.out.println("finalize called");
	}

	@Override // ensures we are overriding. for compile time safety only no other use change name and see.
	public String getEmpName() {
		System.out.println("i can override getEmpName");
		return "MYNAME";
	}

	// public final void finalMeth(){} //A- Final method cannot be overridden
	public static void main(String[] args) throws ArithmeticException {	//A- Telling jvm(implementing method) to handle the thrown exception.
		try {
			G_ExceptionFinalFinallyFinalize g_ExceptionFinalFinallyFinalize = new G_ExceptionFinalFinallyFinalize(555);
			System.out.println("in try");
			System.out.println("finalVariable : " + g_ExceptionFinalFinallyFinalize.finalVariable);
			// g_ExceptionFinalFinallyFinalize.finalVariable = "CANNOT_ASSIGN_TO_FINAL_VARIABLE"; //A- CANNOT_ASSIGN_TO_FINAL_VARIABLE
			System.out.println("getEmpId : " + g_ExceptionFinalFinallyFinalize.getEmpName());
			g_ExceptionFinalFinallyFinalize.finalMeth();//A- Final Method can be called ,but not overridden

		} catch (Exception e) {
			// TODO: handle exception
		} /*
		 catch (NumberFormatException ex){ //A- This is not allow to compile, because Exception is the biggest basket and should be at the last. 
		 }*/
		 finally {
			System.gc();//A- Called at the end of the pgm. Before this finalize block is called. Garbage collection ?
			System.out.println("finally executed for sure. " + "\n Catch is not mandatory if finally block is included. \n" + "we cannot have only try block ");
			//A- Catch Block is not mandatory if finally block is included.
		}
	}
}
// Creating a singelton class
final class Singleton {
    private static final Singleton INSTANCE = new Singleton();

    private Singleton() {}

    public static Singleton getInstance() {
        return INSTANCE;
    }
}
/*
final
======		
1)	Final is used to apply restrictions on class, method and variable. 
	-Final class can't be inherited, 
	-final method can't be overridden and 
	-final variable value can't be changed.	
2)	Final is a keyword.		
3) final keyword is used to achieve high level of security while coding.

finally
========
1) Finally is used to place important code, it will be executed whether exception is handled or not.
2) Finally is a block.

finalize
=========
1) Finalize is used to perform clean up processing just before object is garbage collected.
2) Finalize is a method.
*/