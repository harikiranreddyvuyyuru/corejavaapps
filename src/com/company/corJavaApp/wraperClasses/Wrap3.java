package com.company.corJavaApp.wraperClasses;

public class Wrap3 {

	public static void main(String[] args) {
		
		Byte B = new Byte("77");
		int i = B.intValue();
		System.out.println(i);
		
		Short S = new Short("99");
		i = S.intValue();
		System.out.println(i);
		
		Integer I = new Integer("33");
		i = I.intValue();
		System.out.println(i);
		
		Long L = new Long("579");
		i = L.intValue();
		System.out.println(i);
		
		Float F = new Float("79.59");
		i = F.intValue();
		System.out.println(i);
		
		Double D = new Double("989.79");
		i = D.intValue();
		System.out.println(i);
		
	}

}
