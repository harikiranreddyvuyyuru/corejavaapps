package com.company.corJavaApp.wraperClasses;

public class IsNaN {

	public static void main(String[] args) {
		
		double d = 0.0/0.0;
		System.out.println(Double.isNaN(d));
		
		d = Math.sqrt(-1.2);
		System.out.println(Double.isNaN(d));
		
		float f = 0.0f/0.0f;
		System.out.println(Float.isNaN(f));
		
		f = 0 * Float.POSITIVE_INFINITY;
		System.out.println(Float.isNaN(f));
	}

}
