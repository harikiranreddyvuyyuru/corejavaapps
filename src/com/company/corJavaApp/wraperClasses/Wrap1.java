package com.company.corJavaApp.wraperClasses;

public class Wrap1 {

	public static void main(String[] args) {
		Byte B=Byte.valueOf((byte) 234);
		System.out.println(B);
		
		Short S=Short.valueOf((short) 579);
		System.out.println(S);
		
		Integer I=Integer.valueOf(79);
		System.out.println(I);
		
		Long L = Long.valueOf(9999);
		System.out.println(L);
		
		Float F = Float.valueOf(17.3f);
		System.out.println(F);
		
		Double D = Double.valueOf(22.5d);
		System.out.println(D);
		
		Boolean BLN = Boolean.valueOf(true);
		System.out.println(BLN);
		
		BLN = Boolean.valueOf(false);
		System.out.println(BLN);
		
		Character H = Character.valueOf('H');
		System.out.println(H);
		
		}

}
