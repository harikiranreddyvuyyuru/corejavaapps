package com.company.corJavaApp.wraperClasses;

public class IsInfinite {

	public static void main(String[] args) {

		double d = Double.POSITIVE_INFINITY /0.0;
		System.out.println(Double.isInfinite(d));
		
		d = Double.NEGATIVE_INFINITY /0.0;
		System.out.println(Double.isInfinite(d));
		
		float f = Float.POSITIVE_INFINITY * 2.2f;
		System.out.println(Float.isInfinite(f));
		
		f = Float.NEGATIVE_INFINITY * 4.12f;
		System.out.println(Float.isInfinite(f));
	}

}
