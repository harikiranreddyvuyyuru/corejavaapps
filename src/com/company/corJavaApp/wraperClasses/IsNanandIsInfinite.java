package com.company.corJavaApp.wraperClasses;

public class IsNanandIsInfinite {

	public static void main(String[] args) {
		
		double d1 = Double.NaN;
		System.out.println(d1);
		
		double d2 = Double.POSITIVE_INFINITY;
		System.out.println(d2);
		
		double d3 = Double.NEGATIVE_INFINITY;
		System.out.println(d3);
		
		float f1 = Float.NaN;
		System.out.println(f1);
		
		float f2 = Float.POSITIVE_INFINITY;
		System.out.println(f2);
		
		float f3 = Float.NEGATIVE_INFINITY;
		System.out.println(f3);
		

	}

}
