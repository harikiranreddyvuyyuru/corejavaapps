package com.company.corJavaApp.wraperClasses;

public class Wrap4 {

	public static void main(String[] args) {
      
		Byte B = new Byte("79");
		long l = B.longValue();
		System.out.println(l);
		
		Short S = new Short("29");
		l = S.longValue();
		System.out.println(l);
		
		Integer I = new Integer("89");
		l = I.longValue();
		System.out.println(l);
		
		Long L = new Long("9889");
		l = L.longValue();
		System.out.println(l);
		
		Float F= new Float("57.99");
		l=F.longValue();
		System.out.println(l);
		
		Double D = new Double("9889.99");
		l = D.longValue();
		System.out.println(l);
		
	}

}
