package com.company.corJavaApp.wraperClasses;

public class Wrap2 {

	public static void main(String[] args) {

		Byte B = Byte.valueOf("101101", 2);
		System.out.println(B);

		Short S = Short.valueOf("12043", 6);
		System.out.println(S);

		Integer I = Integer.valueOf("3579ABD", 16);
		System.out.println(I);

		Long L = Long.valueOf("VHKRAK", 36);
		System.out.println(L);

	}

}
