package com.company.corJavaApp;
//QQ What is Method? Deceleration Rules? Types of Methods?
public class B_Methods {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
	}

	void methodDemo() {
		// 1)Method with No Argument and No return Type.
	}

	void methodDemo(String name, int id) {
		// 2)Method with Arguments and No return type(meaning Output)
	}

	String methodDemo2() {
		// 3)method with No Arguments and return type.
		return "I can return some data";
	}

	String methodDemo2(String name, int id) {
		// 4)method with Arguments and return type.
		return "I can return some data";
	}

}

/* A Method takes an input as arguments process the data and may or may not return the data. 
 * A method can call one or many methods.
 * 4 Types of methods are available.
 * Ensure to follow camelCase rule.
 * Method represents an action.It's a kind of verb.Multiple methods are written to organize the code with meaning full services/names
 */