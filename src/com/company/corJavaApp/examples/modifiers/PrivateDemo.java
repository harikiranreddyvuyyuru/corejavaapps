package com.company.corJavaApp.examples.modifiers;

public class PrivateDemo {
	
	private String first_name;
	private String last_name;
	//let Variable name be firstName;
	
	void show(){
		
		System.out.println("Frist Name:="+first_name);
		System.out.println("Last Name:="+last_name);
	}

	public static void main(String[] args) {
		
		PrivateDemo obj=new PrivateDemo();
		
		obj.first_name="Hari";
		obj.last_name="Reddy";
		obj.show();
		// TODO Auto-generated method stub

	}

}
