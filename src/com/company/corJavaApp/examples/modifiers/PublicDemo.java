package com.company.corJavaApp.examples.modifiers;

public class PublicDemo {

	public int a;
	public int b;
	
	public void show(){
		
		System.out.println("a="+a);
		System.out.println("b="+b);
	}
	
	public static void main(String[] args) {
		
		PublicDemo obj=new PublicDemo();
		
		obj.a=2;
		obj.b=1;
		obj.show();
		// TODO Auto-generated method stub

	}

}
