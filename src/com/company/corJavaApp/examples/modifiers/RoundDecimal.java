package com.company.corJavaApp.examples.modifiers;

import java.text.DecimalFormat;

public class RoundDecimal {

	public static void main(String[] args) {
		
		double number=9.345322884;
		DecimalFormat df1 = new DecimalFormat("#.##");
		System.out.println(number +"is rounded to:" + df1.format(number));
		
		DecimalFormat df2 = new DecimalFormat("#.###");
		System.out.println(number + "is rounded to:" + df2.format(number));
		
		number=2.344599950;
		
		DecimalFormat df3 = new DecimalFormat("#.####");
		System.out.println(number + "is rounded to:" + df3.format(number));
		
		DecimalFormat df4 = new DecimalFormat("#.#####");
		System.out.println(number + "is rounded to:" + df4.format(number));
		
		
		
		
		
		// TODO Auto-generated method stub

	}

}
