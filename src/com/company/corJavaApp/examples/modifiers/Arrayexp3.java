package com.company.corJavaApp.examples.modifiers;

import java.util.ArrayList;
import java.util.ListIterator;

public class Arrayexp3 {

	public static void main(String[] args) {
		
		ArrayList<String> list=new ArrayList<String>();
		
		list.add("ArrayList Advantages");
		
		list.add("ArrayList benefits");
		
		list.add("ArrayList in java");
		
		ListIterator iterator = list.listIterator();
		
		System.out.println("**ArrayList Elements in forward direction**");
	         
		while(iterator.hasNext())
		{
			System.out.println(iterator.next());
		}
		System.out.println("**ArrayList Elements in Backward direction**");
		
		while(iterator.hasPrevious())
		{
			System.out.println(iterator.previous());
		}
		// TODO Auto-generated method stub

	}

}
