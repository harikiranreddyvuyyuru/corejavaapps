package com.company.corJavaApp.examples.modifiers;

import java.util.ArrayList;
import java.util.List;

public class ArrayListToArray {

	public static void main(String[] args) {
		List<Integer> list = new ArrayList<Integer>();

		list.add(90);
		list.add(80);
		list.add(60);
		list.add(30);
		Object[] integersArray = list.toArray();
		String[] intarray = new String[integersArray.length];
		int i = 0;
		for (Object n : integersArray) {
			intarray[i++] = n.toString();
			// System.outprintln(i);
		}
		for (String it : intarray) {
			System.out.println(it);
		}
		System.out.println("==========================================");
		for (int im = 0; im < intarray.length; im++) {
			System.out.println(intarray[im]);
		}
	}

}
