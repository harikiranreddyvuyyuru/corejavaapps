package com.company.corJavaApp.examples.modifiers;

public class Boolean {

	public static void main(String[] args) {
		boolean x = true;
		boolean y = false;
		boolean z = x ^ y;

		System.out.println(z);

		System.out.println(false ^ false);
		System.out.println(true ^ true);

		System.out.println(1 ^ 0);
		// TODO Auto-generated method stub

	}

}
