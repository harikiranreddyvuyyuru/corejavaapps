package com.company.corJavaApp.examples.modifiers;

public class ProtectedDemo {
	
	protected int x;
	protected int z;
	
	protected void show(){
		
		System.out.println("x="+x);
		System.out.println("z="+z);
	}

	public static void main(String[] args) {
		
		ProtectedDemo obj= new ProtectedDemo ();
		
		obj.x=20;
		obj.z=9;
		obj.show();
		// TODO Auto-generated method stub

	}

}
