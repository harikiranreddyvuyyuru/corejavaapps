package com.company.corJavaApp.examples.javaBasics;

public class StaticMethod {
	public static int myId = 31;
	static void show() {
		System.out.println("show method called@@@@@@@@@@@@@@");
	}

	static void print() {
		System.out.println("print method called");
	}

	static void display() {
		System.out.println("display method called");
	}

	public static void main(String[] args) {
		System.out.println("main method called");

		show();
		print();
		display();

		// TODO Auto-generated method stub

	}

}
