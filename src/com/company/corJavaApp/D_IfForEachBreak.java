package com.company.corJavaApp;

import java.util.ArrayList;
import java.util.List;

import com.corejava.utils.Employee;
//QQ When break is used in realtime.
public class D_IfForEachBreak {

	public static void main(String[] args) {

		int value1 = 1;
		int value2 = 2;
		int valueResult;
		List employeesList = new ArrayList<>();
		for (int i = 0; i < 5; i++) {
			employeesList.add(new Employee(i, "Empl" + i));
			employeesList.add(i+" A sting Added");
		}

		for (Object object : employeesList) { //foreach is an enhanced for loop. type foreach and Ctrl+Space, eclipse will help you.
			System.out.println("object  : " + object.toString());//A- Same output if you remove toString() as we implemented it
			if(object.toString().equals("2 A sting Added")){
				System.out.println("equals triggered");
			} else if(object.toString().equals("2 A sting Added"))
				System.out.println("else if will not be triggered");//A- Not triggered also note that no flower baces needed if writing only one line code.
			  else
				System.out.println("Not triggered");
				
			
		}//end of for loop


		if (employeesList != null && !employeesList.isEmpty()) {
			System.out.println(employeesList);
			System.out.println( "employeesList Size  : " + employeesList.size() );
			
		}
		//==============================================================================
		
		//Understanding break and continue
		for (int i = 0; i < 8; i++) {
			System.out.println("Inside loop i = " + i);
			if (i == 5) {
				System.out.println("......REACHED continue... i=" + i);
				continue;
			}
			// more loop code, that won't be reached when the above if test is
			// true.
			System.out.println("after continue.... i=" + i);
		}
		outer :
		for (int i = 0; i < 8; i++) {
			System.out.println("Inside loop i = " + i);
			for (int j = 0; j < 8; j++) {
				System.out.println("J=" + j);
				if (j == 5) {
					System.out.println("......REACHED break... i=" + i);
					//break;//this will exit from j only
					break outer;// this will exit completely.
				}
			}
		
			System.out.println("after break.... i=" + i);
		}
		//--------------------------------------------------
		
	}

}
