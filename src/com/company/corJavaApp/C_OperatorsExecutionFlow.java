package com.company.corJavaApp;
//QQ What is java assignment order String s= a+b+c left to right or right to left ? Which Arithmetic Operators has High Priority ? 
//QQ What is casting and widening in java? 
//QQ How pre- fix and post fix increment works?
//QQ Explain about Casting (up/down) and instanceof operator
//QQ ternary operator?
//1AA
public class C_OperatorsExecutionFlow {

	public static void main(String[] args) {
		// --------------------- arithmaticResult Begin ----------------------------
		System.out.println("------arithmaticResult------");
		int arithmaticResult = 1 + 2;
		System.out.println("1 + 2 = " + arithmaticResult);
		int original_result = arithmaticResult;

		arithmaticResult = arithmaticResult - 1;
		System.out.println(original_result + " - 1 = " + arithmaticResult);
		original_result = arithmaticResult;

		arithmaticResult = arithmaticResult * 2;
		System.out.println(original_result + " * 2 = " + arithmaticResult);
		original_result = arithmaticResult;

		arithmaticResult = arithmaticResult / 2;
		System.out.println(original_result + " / 2 = " + arithmaticResult);
		original_result = arithmaticResult;

		arithmaticResult = arithmaticResult + 8;
		System.out.println(original_result + " + 8 = " + arithmaticResult);
		original_result = arithmaticResult;

		arithmaticResult = arithmaticResult % 7;
		System.out.println(original_result + " % 7 = " + arithmaticResult);
	//A-	* / % + - are the mathematical operators
	//A-	* / % have a higher precedence than + or -
		int a=1;
		int b=2;
		int c=3;
		int d=4;
		double myVal = a + b % d - c * d / b; //A- we have widened int to double(Widening small to big scope.)
		System.out.println("myVal : " +myVal);
		//same as:
		double myModifiedVal = (a+(b%d)) - ((c*d) / b); //Do Ctrl+Shif+F by selecting this line.
		System.out.println("myModifiedVal : " +myModifiedVal);
		
		// ------------------------------- arithmaticResult End 1 -----------------
		// -------------------Unary Operators Begin-------------
		System.out.println("------Unary Operation Results------");
		int unaryResult = +1;
		System.out.println(unaryResult);

		unaryResult--;
		System.out.println(unaryResult);

		unaryResult++;
		System.out.println(unaryResult);

		unaryResult = -unaryResult;
		System.out.println(unaryResult);

		boolean success = false;
		System.out.println(success);
		System.out.println(!success);
		// -------------------unary operator end---------
		//--------- prefix and post fix. understand the trick----
		System.out.println("------Increment operation results------");
		int i = 3;
        // prints 4
        System.out.println(i);
        ++i;			   
        System.out.println(i);
        System.out.println(++i);
        // prints 6
        int j= 0;
        j=j + ++i;
        System.out.println("j : "+j);
        // prints 7
        System.out.println(i);
        
        //----------------------------------------------------------
        System.out.println("------Equality and Relational Operators---------");
        int value1 = 1;
        int value2 = 2;
        int valueResult ;
        if(value1 == value2)
            System.out.println("value1 == value2");
        if(value1 != value2)
            System.out.println("value1 != value2");
        if(value1 > value2)
            System.out.println("value1 > value2");
        if(value1 < value2)
            System.out.println("value1 < value2");
        if(value1 <= value2)
//            System.out.println("value1 <= value2");bnjhinm
      //----------------------------------------------------------
        System.out.println("------The Conditional Operators---------");
        if((value1 == 1) && (value2 == 2)) //A- In AND operation if 1st condition is FALSE it need not check for next condition.Mainly used to do null check.
            System.out.println("value1 is 1 AND value2 is 2");
        if((value1 == 1) || (value2 == 1)) //A- In OR operation if  1st condition is TRUE it need not check for next condition.
            System.out.println("value1 is 1 OR value2 is 1");
        boolean someCondition = true;
        valueResult = someCondition ? value1 : value2;
        System.out.println("valueResult from ternary operator ? : "+valueResult);
        //----------------------------------------------------------
       
        System.out.println("------ The Type Comparison Operator instanceof---------");
		//A- Comparison Operator instanceof
        /*
		 * The instanceof operator compares an object to a specified type. 
		 * You can use it to test if an object is an instance of a class, an instance of a subclass, 
		 * or an instance of a class that implements a particular interface.
		 * The following program, InstanceofDemo, defines a parent class (named Parent), a simple interface (named MyInterface), and a child class
		 * (named Child) that inherits from the parent and implements the
		 * interface.
		 */
        Parent obj1 = new Parent();
        Parent obj2 = new Child();
        Object childObject = new Parent(); // A- Object class is the base class or super class for all the classes. Any Object you create extends from Object Class.
      //  Child child = new Parent();//cannot assign like above, compiler will not accept
     //   Child child = (Child) new Parent(); //Compiles successfully but ClassCastException is thrown at runtime, cannot force a parent to become a child ! 
       if(obj2 instanceof Child){
    	   Child child3 =(Child) obj2;//A- A child instance is assigned to Parent variable(obj2) and now it's assigned back to child3 by casting it.   
    	   System.out.println("Casted. Note that obj2 is of parent type");
       }
         
        System.out.println("childObject : " + childObject);
        System.out.println("obj1 instanceof Parent: " + (obj1 instanceof Parent));
        System.out.println("obj1 instanceof Child: " + (obj1 instanceof Child));
        System.out.println("obj1 instanceof MyInterface: " + (obj1 instanceof MyInterface));
        System.out.println("obj2 instanceof Parent: " + (obj2 instanceof Parent));
        System.out.println("obj2 instanceof Child: " + (obj2 instanceof Child));
        System.out.println("obj2 instanceof MyInterface: " + (obj2 instanceof MyInterface));
        //----------------------------------------------------------
	}// psvm end

}//Class end

class Parent {}
class Child extends Parent implements MyInterface {}
interface MyInterface {}


/*references: A- https://docs.oracle.com/javase/tutorial/java/javaOO/variables.html
 * http://javaconceptoftheday.com/type-casting-in-java/

Operators		|	Precedence
================================================
postfix			|	expr++ expr--
unary			|	++expr --expr +expr -expr ~ !
multiplicative	|	* / %
additive		|	+ -
shift			|	<< >> >>>
relational		|	< > <= >= instanceof
equality		|	== !=
bitwise	AND		|	&
bitwise exclusive OR |	^
bitwise inclusive OR | |
logical AND		|	&&
logical OR		|	||
ternary			|	? :
assignment		|	= += -= *= /= %= &= ^= |= <<= >>= >>>=*/

