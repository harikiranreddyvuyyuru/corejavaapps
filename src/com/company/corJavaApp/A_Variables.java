package com.company.corJavaApp;

/*QQ Explain what is pass by value?
QQ What are available variable types an methods in java?
QQ Why main method is static?
QQ Difference between static and instance variables?
QQ Explain the variable and method Declaration rules.?
*/		
		
public class A_Variables {

	public static final String AUTO_EXTRACTED_CONSTANT = "auto extracted"; //1)Static Variable(has final KEYWORD this is a constant)
	String instanceVariable = "itsInstanceVariable";//2)instance variable
static {
	System.out.println();
}
	public static void main(String[] args) {// Declaration and Definition of a Method/function
			System.out.println("Hello World.");
			// Legal
			int _a = 0; // Declaration of a Variable/identifier, Not Defined, so cannot use it
			System.out.println(_a);
			_a=6;
			System.out.println(_a);
			int $c;// 3) Local Variable, within a method.
			int ______2_w;
			int _$;
			String SUGGEST_CONSTANTS_ONLY_WITH_UNDERSCORE;
			String goodNammingConventionWithCamelCase;
			// Illegal
			// int :b;
			// int -d;
			// int e#;
			// int .f;
			// int 7g;
			// int return; //We cannot Use reserved Key Words.
			// Ensure to give proper meaning-full names
			byte nextInStream;
			short hour = 0;
			long totalNumberOfStars;
			float reactionTime;
			double itemPrice;
			//------Assignments from right to left
			int x= 1;
			int y= 2;
			int z= 3;
			System.out.println("x : "+ x + " y : " + y + " z :" + z);
			x=y=z;//assigns from right to left
			System.out.println("x : "+ x + " y : " + y + " z :" + z);
			
			Object empObj = null;
			empObj= new Object();
			empObj= myFuction(empObj);
			String localVariable = "my local variable";
			localVariable = doMath(localVariable );
			System.out.println("localVariable : "+ localVariable);
	}

	private static Object myFuction(Object empObj) {
	
		return empObj;
		// TODO Auto-generated method stub
		//emObj.setn
		
	}

	private static String doMath(String parameterVariable) {
		System.out.println("parameterVariable : "+ parameterVariable);
		int temp = 58;
		int result = temp * 2;
		parameterVariable = "i am modified in doMath method.";
		System.out.println("Within Method : " + parameterVariable);
		String extractedData = AUTO_EXTRACTED_CONSTANT;// Alt+Shif+T and check as needed.
		
	return parameterVariable;
	}

}

/*QQ Declaration and Definition Assignment in Java:

Declaration: You are declaring that something exists, such as a class, function or variable. You don't say anything about what that class or function looks like, you just say that it exists.

Definition: You define how something is implemented, such as a class, function or variable, i.e. you say what it actually is.
In Java, there is little difference between the two, and formally speaking, a declaration includes not only the identifier, but also it's definition. Here is how I personally interpret the terms in detail:

Classes: Java doesn't really separate declarations and definitions as C/C++ does (in header and cpp files). You define them at the point where you declare them.
Functions: When you're writing an interface (or an abstract class), you could say that you're declaring a function, without defining it. Ordinary functions however, 
are always defined right where they are declared. See the body of the function as its definition if you like.

Variables: A variable declaration could look like this:
int x;
(you're declaring that a variable x exists and has type int) either if it's a local variable or member field. In Java, there's no information left about x to define,
 except possible what values it shall hold, which is determined by the assignments to it.

QQ Assignment and instantiation?
*/



/**
A variable is a container that holds values that are used in a Java program.
To be able to use a variable it needs to be declared. Declaring variables is normally the first thing that happens in any program.
TYPES OF VARIABLES::
-------------------
//  1) Static Variable/Class Variable/Static Field A variable/field that is declared with static MODIFIER is called static variable.
//		No object needed to access it.It tells the compiler that this will have only ONE value at any point of time.
	
//	2) Member variables/Instance variable/Non-Static Fields .Located in class level. Accessed through objects .
// 		known as instance variables because their values are unique to each instance of a class (to each object, in other words); the currentSpeed of one bicycle is independent from the currentSpeed of another.
	
//	3) Local Variable : Variables in a method or block of code�these are called local variables.
// 		local variables are only visible to the methods in which they are declared; they are not accessible from the rest of the class.
	
//	4) Parameter : Variables in method declarations�these are called parameters.

Legal Identifiers/Rules::
-------------------------
 Here are the rules you do need to know:
>> Identifiers must start with a letter, a currency character ($), or a connecting character such as the underscore ( _ ). Identifiers cannot start with a number!
>> After the first character, identifiers can contain any combination of letters, currency characters, connecting characters, or numbers.
>> In practice, there is no limit to the number of characters an identifier can contain.
>> You can't use a Java keywords as an identifier. 
>> Identifiers in Java are case-sensitive; foo and FOO are two different identifiers.
QQ Choosing Variable Names
>> Always give your variables meaningful identifiers. If a variable holds the price of a book, then call it something like "bookPrice"(camelCase wording). 
If each variable has a name that makes it clear what it's being used for, it will make finding errors in your programs a lot easier.
*/


